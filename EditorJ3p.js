import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { formValues } from 'redux-form'
import { IframeField } from 'client-react/components/fields'
import appConf from 'server/config'

const { version, pluginsOptions } = appConf

// page de l'éditeur j3p à insérer en iframe
// import iframeSrc from './public/editgraphe.html'
// pas trouvé comment faire fonctionner html-loader avec pnpm (charge en iframe une page contenant
// module.exports = __webpack_public_path__ + "9f2cc6457248b5ee981bc40aaded39bd.html";
// au lieu de la page 9f2cc6457248b5ee981bc40aaded39bd.html

class EditorJ3p extends Component {
  /**
   * Appelée par le onLoad de l'iframe
   * @param {HTMLElement} iframe Iframe présente dans le DOM
   */
  onIframeLoaded (iframeRef, input) {
    const parametres = typeof this.props.parametres === 'string' ? JSON.parse(this.props.parametres) : this.props.parametres
    this.iframeRef = iframeRef
    const options = pluginsOptions.j3p || {}
    iframeRef.current.contentWindow.load({ parametres }, options, input)
  }

  componentDidUpdate (prevProps) {
    // on rafraîchit l'iframe après une sauvegarde réussie
    if (this.props.submitSucceeded && !prevProps.submitSucceeded) {
      if (this.iframeRef && this.iframeRef.current) {
        this.iframeRef.current.contentWindow.location.reload()
      }
    }
  }

  render () {
    return (
      <IframeField
        label="Édition des paramètres j3p"
        allowManualEdition
        onLoad={this.onIframeLoaded.bind(this)}
        src={`/plugins/j3p/editgraphe.html?${version}`}
        name="parametres"
      />
    )
  }
}

EditorJ3p.propTypes = {
  parametres: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.string
  ]),
  submitSucceeded: PropTypes.bool
}

const mapStateToProps = ({ form: { ressource: { submitSucceeded } } }) => ({ submitSucceeded })

export default connect(mapStateToProps, {})(
  formValues({ parametres: 'parametres' })(EditorJ3p)
)
