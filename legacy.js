/**
 * Le plugin legacy de vite génère pas mal de code dans le index.html qu'il construit
 * On récupère ici ce qu'il a généré pour le réinjecter dans la page courante
 * @todo à finir, ou plus probablement laisser tomber
 * @param {string} baseUrl
 */
export default async function prepareForLegacyBrowser (baseUrl) {
  const url = `${baseUrl}index.html`
  const response = await fetch(url)
  if (!response.ok) throw Error(`Erreur HTTP ${response.status} ${response.statusText} sur ${url}`)
  const html = await response.text()
  // console.log('on récupère la page', html)
  const head = document.getElementsByTagName('head')[0]
  let isHead = false
  for (const line of html.split('\n')) {
    if (/<head>/.test(line)) {
      isHead = true
      continue
    }
    if (/<\/head>/.test(line)) {
      isHead = false
      continue
    }
    const parent = isHead ? head : document.body
    const chunks = /<script([^>]+)>(.*)<\/script>/.exec(line)
    if (chunks) {
      console.debug('ligne à étudier', line)
      const [, attrs, content] = chunks
      // on ne veut prendre que les scripts avec du code et celui qui charge les polyfills
      if (!content && !/polyfills-legacy/.test(attrs)) continue
      console.debug('ligne à ajouter', line)
      const script = document.createElement('script')
      const reAttr = /\s+([^\s=]+)(="[^"]+")?/g
      let pieces
      // eslint-disable-next-line no-cond-assign
      while (pieces = reAttr.exec(attrs)) {
        let [, attr, value] = pieces
        if (value) {
          value = value.substring(2, value.length - 1) // ne conserve que … dans ="…"
          if (['href', 'src', 'data-src'].includes(attr)) {
            value = baseUrl + value.replace(/^\.\//, '')
          }
          // console.log('attr', attr, value)
          script.setAttribute(attr, value)
        } else {
          // console.log('attr boolean', attr)
          script[attr] = true
        }
      }
      if (content) script.innerHTML = content
      console.debug('ajout de', script.outerHTML, 'à', parent, isHead)
      parent.appendChild(script)
      continue
    }
    console.debug('ligne ignorée', line)
  }
  // console.log('fin prepareForLegacyBrowser')
}
