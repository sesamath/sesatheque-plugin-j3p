import log from 'sesajstools/utils/log'
import sjtUrl from 'sesajstools/http/url'
import xhr from 'sesajstools/http/xhr'
import dom from 'sesajstools/dom'
// ça c'est sur la sesathèque
import page from 'client/page/index'
// import prepareForLegacyBrowser from './legacy'

const { addJs } = dom

/**
 * Affiche la ressource dans l'élément d'id mepRess
 * @service plugins/j3p/display
 * @param {Ressource}      ressource  L'objet ressource
 * @param {displayOptions} options    Les options après init
 * @param {errorCallback}  next       La fct à appeler quand le swf sera chargé
 */
export const display = (ressource, options, next) => {
  /**
   * Chargera la ressource quand on aura éventuellement récupéré lastResultat
   */
  async function load () {
    const isDev = options.isDev || /\.sesamath\.dev$/.test(window.location.hostname)
    const isLocal = !isDev && (options.isLocal || /\.local$/.test(window.location.hostname) || /^localhost$/.test(window.location.hostname))
    const tld = isDev ? 'dev' : (isLocal ? 'local' : 'net')
    const urlBaseJ3p = `https://j3p.sesamath.${tld}/`
    // await prepareForLegacyBrowser(urlBaseJ3p)
    addJs(`${urlBaseJ3p}build/loader.js?${Math.floor(Date.now() / 10000000)}`, function () { // on ajoute un suffixe pour changer de cache toutes les 10k s soit environ 3h
      if (typeof j3pLoad !== 'function') return next(Error('Pb de chargement j3p'))
      /* global j3pLoad */
      try {
        // on cache toujours le titre
        page.hideTitle()
        // on lui donne nos params
        const j3pOptions = {
          baseUrl: urlBaseJ3p,
          graphe: ressource.parametres.g,
          editgraphes: ressource.parametres.editgraphes
        }
        if (options.resultatCallback) {
          j3pOptions.resultatCallback = options.resultatCallback
        }
        if (options.lastResultat) {
          j3pOptions.lastResultat = options.lastResultat
        }
        j3pLoad(options.container, j3pOptions, next)
      } catch (error) {
        page.addError(error)
      }
    })
  }

  try {
    // log('j3p.display avec ressource et options', ressource, options)
    // les params minimaux
    if (!ressource.oid || !ressource.titre || !ressource.parametres || !ressource.parametres.g) {
      throw new Error('Ressource incomplète')
    }
    if (!options.container || !options.errorsContainer) throw new Error('Paramètres manquants')
    if (!Array.isArray(ressource.parametres.g)) throw Error('Le graphe n’est pas un tableau')

    const lastResultUrl = sjtUrl.getParameter('lastResultUrl')
    if (lastResultUrl) {
      // log('on va chercher un lastResultat sur ' + lastResultUrl)
      xhr.get(lastResultUrl, { responseType: 'json' }, function (error, data) {
        if (error) {
          log.error(error)
          page.addError('Impossible de récupérer le dernier résultat')
        } else if (data) {
          if (data.success) {
            if (data.resultat) options.lastResultat = data.resultat
          } else {
            log.error("l'appel de lastResultat a échoué", data)
          }
        }
        // mais quoi qu'il arrive on charge
        load()
      })
    } else {
      load()
    }
  } catch (error) {
    page.addError(error)
  }
}
