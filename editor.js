import editor from './EditorJ3p'
import type from './type'

const saveHook = ({
  parametres,
  ...others
}) => {
  const {
    g = [],
    editgraphes: {
      positionNodes = [],
      titreNodes = []
    } = {}
  } = (typeof parametres === 'string') ? JSON.parse(parametres) : parametres

  const maxNumericId = g.reduce((max, node) => {
    const numId = Array.isArray(node) && Number(node[0])
    return Number.isFinite(numId) ? Math.max(max, numId) : max
  }, 1)
  // on retourne un entier en string, car j3p s'attend à des nodeId numériques, mais ça devrait plutôt être du "nodeN"
  const nextNodeId = () => String(Math.max(g.length, maxNumericId + 1))

  const missingSuccessors = []

  for (const [index, [nodeId, sectionName, sectionOpts]] of g.entries()) {
    if (sectionName === 'fin') continue
    let addedNodeId
    // on vérifie qu'il y a un nn dans les branchements et que ce nn existe dans le graphe
    if (!Array.isArray(sectionOpts) || !sectionOpts.some(opts => opts && Boolean(opts.nn) && g.some(node => node[0] === String(opts.nn)))) {
      missingSuccessors.push(nodeId)
      addedNodeId = nextNodeId()
      // ajout du nœud fin généré
      g.push([
        addedNodeId,
        'fin',
        []
      ])
      // et ajout du branchement vers ce nœud fin en premier (aucun suivant, donc si y'a qqchose dans sectionOpts c'est des params)
      sectionOpts.unshift({
        nn: addedNodeId,
        score: 'sans+condition',
        conclusion: 'Fin de l‘activité'
      })
      const [x, y] = positionNodes[index] ?? [index * 20, index * 100]
      positionNodes.push([
        x + 180,
        y + 60
      ])
      titreNodes.push('Fin')
    }
  }

  if (missingSuccessors.length) {
    if (missingSuccessors.length === 1) {
      // eslint-disable-next-line no-alert
      alert('Le noeud ' + missingSuccessors[0] + ' ne comportait pas de branchement, un branchement vers un noeud Fin a été ajouté automatiquement.')
    } else {
      // eslint-disable-next-line no-alert
      alert('Les noeuds ' + missingSuccessors.join(', ') + ' ne comportaient pas de branchement, un branchement vers un noeud Fin a été ajouté automatiquement.')
    }
  }

  return {
    ...others,
    parametres: {
      g,
      editgraphes: {
        positionNodes,
        titreNodes
      }
    }
  }
}

export { editor, type, saveHook }
