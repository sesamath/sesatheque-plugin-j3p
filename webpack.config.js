const path = require('path')
const { URL } = require('url')

const CopyWebpackPlugin = require('copy-webpack-plugin')

const to = 'plugins/j3p/[name][ext]' // surtout pas de contenthash ici, faut préserver le nom

// c'est la config de la sesatheque qui nous compile qui va éventuellement imposer le j3p de dev
// mais on teste quand même la présence de ce fichier
module.exports = ({ baseUrl }) => {
  const patterns = [{
    from: path.resolve(__dirname, 'public', 'j3p.gif'),
    to
  }]
  // pour le html faut éventuellement transformer
  const pattern = {
    from: path.resolve(__dirname, 'public', 'editgraphe.html'),
    to
  }
  const { hostname } = new URL(baseUrl)
  if (/(sesamath\.dev|localhost|\.local)$/.test(hostname)) {
    pattern.transform = (content) => content.toString().replace(/sesamath\.net/g, 'sesamath.dev')
  }
  patterns.push(pattern)
  return {
    entries: {},
    plugins: [new CopyWebpackPlugin({ patterns })],
    rules: []
  }
}
